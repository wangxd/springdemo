package com.example.dao;

import com.example.dto.Page;
import com.example.pojo.KgcUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Auther: wangxiaodan
 * @Date:2021/3/17 10:32
 * @Description:
 **/
public interface KgcUserMapper {
    KgcUser queryUserByName(@Param("userName") String queryUserByName);

    List<KgcUser> queryUserList(Map<String, Object> map);

    int queryUserCountByParam(@Param("name") String name, @Param("state") Integer state);

    List<KgcUser> queryUserByPage(@Param("page") Page page);
}
