package com.example.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther: wangxiaodan
 * @Date:2021/4/1 9:58
 * @Description:
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Page {
    private int startPage; //起始页
    private int pageSize;//每页显示的条数
    private int totalSize; //数据总条数
}
