package com.example;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@MapperScan(basePackages = "com.example.dao")
@EnableAsync
public class SpringbootMailDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootMailDemoApplication.class, args);
    }

}
