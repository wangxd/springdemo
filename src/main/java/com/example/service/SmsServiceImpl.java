package com.example.service;

import com.cloopen.rest.sdk.BodyType;
import com.cloopen.rest.sdk.CCPRestSmsSDK;
import com.example.config.SmsConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Set;

/**
 * @Auther: wangxiaodan
 * @Date:2021/3/26 10:41
 * @Description:
 **/
@Service
public class SmsServiceImpl implements SmsService {
    @Autowired
    private SmsConfig smsConfig;
    @Override
    public void sendSms(String to) {
        CCPRestSmsSDK sdk = new CCPRestSmsSDK();
        sdk.init(smsConfig.getServerip(), smsConfig.getPort());
        sdk.setAccount(smsConfig.getAccountsid(), smsConfig.getAccounttoken());
        sdk.setAppId(smsConfig.getAppid());
        sdk.setBodyType(BodyType.Type_JSON);
//        String templateId= "1"; //模板ID
        String[] datas = {"2345","5"};
        HashMap<String, Object> result = sdk.sendTemplateSMS(to,smsConfig.getTemplateid(),datas);
//        HashMap<String, Object> result = sdk.sendTemplateSMS(to,templateId,datas,subAppend,reqId);
        if("000000".equals(result.get("statusCode"))){
            //正常返回输出data包体信息（map）
            HashMap<String,Object> data = (HashMap<String, Object>) result.get("data");
            Set<String> keySet = data.keySet();
            for(String key:keySet){
                Object object = data.get(key);
                System.out.println(key +" = "+object);
            }
        }else{
            //异常返回输出错误码和错误信息
            System.out.println("错误码=" + result.get("statusCode") +" 错误信息= "+result.get("statusMsg"));
        }
    }

    @Override
    public boolean sendValidateCode(int code, String to, int expire) {
        CCPRestSmsSDK sdk = new CCPRestSmsSDK();
        sdk.init(smsConfig.getServerip(), smsConfig.getPort());
        sdk.setAccount(smsConfig.getAccountsid(), smsConfig.getAccounttoken());
        sdk.setAppId(smsConfig.getAppid());
        sdk.setBodyType(BodyType.Type_JSON);
//        String templateId= "1"; //模板ID
        String[] datas = {String.valueOf(code),String.valueOf(expire)};
        HashMap<String, Object> result = sdk.sendTemplateSMS(to,smsConfig.getTemplateid(),datas);
//        HashMap<String, Object> result = sdk.sendTemplateSMS(to,templateId,datas,subAppend,reqId);
        if("000000".equals(result.get("statusCode"))){
          return true;
        }else{
            //异常返回输出错误码和错误信息
            System.out.println("错误码=" + result.get("statusCode") +" 错误信息= "+result.get("statusMsg"));
            return false;
        }
    }
}
