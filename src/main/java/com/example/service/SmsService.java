package com.example.service;

/**
 * @Auther: wangxiaodan
 * @Date:2021/3/26 10:41
 * @Description:
 **/
public interface SmsService {
    void sendSms(String to);

    /**
     * 发送短信验证码
     * @param code
     * @param to
     * @param expire
     * @return
     */
    boolean sendValidateCode(int code, String to, int expire);
}
