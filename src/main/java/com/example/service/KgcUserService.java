package com.example.service;

import com.example.dto.Page;
import com.example.pojo.KgcUser;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * @Auther: wangxiaodan
 * @Date:2021/3/26 11:51
 * @Description:
 **/
public interface KgcUserService {
    KgcUser queryUserByName(String name);

    List<KgcUser> queryUserByPage(Page page);

    PageInfo<KgcUser> queryUserByPageHelper(Page page);
}
