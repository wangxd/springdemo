package com.example.service;

/**
 * @Auther: wangxiaodan
 * @Date:2021/3/26 9:01
 * @Description:
 **/
public interface SendMailService {
    void sendMail(String msg,String target);

    void complexSendMail(String msg, String target);
}
