package com.example.service;

import com.example.dao.KgcUserMapper;
import com.example.dto.Page;
import com.example.pojo.KgcUser;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Auther: wangxiaodan
 * @Date:2021/3/26 11:51
 * @Description:
 **/
@Service
public class KgcUserServiceImple implements KgcUserService {
    @Autowired
    private KgcUserMapper kgcUserMapper;

    @Override
    public KgcUser queryUserByName(String name) {
        return kgcUserMapper.queryUserByName(name);
    }

    /**
     * 使用自定义拦截器实现分页
     * @param page
     * @return
     */
    @Override
    public List<KgcUser> queryUserByPage(Page page) {
        return kgcUserMapper.queryUserByPage(page);
    }

    /**
     * 使用PageHelper查询分页
     * @param page
     * @return
     */
    @Override
    public PageInfo<KgcUser> queryUserByPageHelper(Page page) {
        PageHelper.startPage(page.getStartPage(), page.getPageSize());
        List<KgcUser> list = kgcUserMapper.queryUserByPage(page);
        PageInfo<KgcUser> info = new PageInfo<>(list);
        return info;
    }
}
