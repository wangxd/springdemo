package com.example.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @Auther: wangxiaodan
 * @Date:2021/3/26 11:42
 * @Description:
 **/
@Controller
public class IndexController {
    /**
     * 跳转到登录页面
     * @return
     */
    @GetMapping("/")
    public String index(){
        return "login";
    }
}
