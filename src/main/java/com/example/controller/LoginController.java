package com.example.controller;

import com.example.dto.ResultDto;
import com.example.pojo.KgcMenu;
import com.example.pojo.KgcUser;
import com.example.service.KgcMenuService;
import com.example.service.KgcUserService;
import com.example.service.SendMailService;
import com.example.service.SmsService;
import com.example.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @Auther: wangxiaodan
 * @Date:2021/3/26 11:45
 * @Description:
 **/
@Controller
public class LoginController {
    @Autowired
    private KgcUserService kgcUserService;
    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private SmsService smsService;

    @Autowired
    private SendMailService sendMailService;

    @Autowired
    private KgcMenuService kgcMenuService;

    /**
     * 用户登录
     * @param userName
     * @param password
     * @return
     */
    @PostMapping("/login")
    @ResponseBody
    public ResultDto login(@RequestParam String userName,@RequestParam String password){
        //1、根据用户名查询用户信息
        KgcUser user = kgcUserService.queryUserByName(userName);
        if(user == null){
            return ResultUtil.returnSuccess(SysCode.SYS_USER_NOT_EXISTS);
        }
        //2、判断密码
        if(!MD5.getMd5(password,32).equals(user.getPwd())){
            return ResultUtil.returnSuccess(SysCode.SYS_USER_PASS_ERROR);
        }
        //3、存入redis中
        redisUtils.set("loginUser",user,SysFinalValue.login_user_expire);//有效期2小时
       return ResultUtil.returnSuccess();
    }

    /**
     * 跳转到登录页面
     * @return
     */
    @GetMapping("/toRegister")
    public String toRegister(){
        return "register";
    }

    /**
     * 发送验证码
     * @param mail
     * @return
     */
    @GetMapping("/getValidateCode")
    @ResponseBody
    public ResultDto getValidateCode(@RequestParam String mail){
        //1、判断mail是否为手机号
        boolean flag = DataValidateUtil.checkPhone(mail);
        int code = MD5.getRandomCode(); //生成验证码
        //将验证码存入到redis中,有效期为120秒
        redisUtils.set(SysFinalValue.validateCode_pre+mail,code,SysFinalValue.validate_code_redis_expire);
        if(flag){
            //发送短信
            boolean result = smsService.sendValidateCode(code, mail,SysFinalValue.validate_code_expire);
            if(result)
                return ResultUtil.returnSuccess();
            return ResultUtil.returnSuccess(SysCode.SYS_SEND_SMS_ERROR);
        }
        //2、判断是否为邮箱
        if(DataValidateUtil.checkMail(mail)){
            //发送邮件
            sendMailService.sendMail(String.valueOf(code),mail);
            return ResultUtil.returnSuccess();
        }
        return ResultUtil.returnSuccess(SysCode.SYS_GET_VALIDATE_CODE_ERROR);
    }

    @PostMapping("/register")
    @ResponseBody
    public ResultDto register(KgcUser user,@RequestParam String mail,@RequestParam String validateCode){
        //校验邮箱是否合法
        if(!DataValidateUtil.checkPhone(mail) && !DataValidateUtil.checkMail(mail))
            return ResultUtil.returnSuccess(SysCode.SYS_GET_VALIDATE_CODE_ERROR);
        //判断验证码和redis中的验证码是否一致
        if(!validateCode.equals(redisUtils.get(SysFinalValue.validateCode_pre+mail)))
            return ResultUtil.returnSuccess(SysCode.SYS_REGISTER_VALIDATECODE_ERROR);
        //TODO 将用户信息存入数据库
        return ResultUtil.returnSuccess();
    }

    /**
     * 跳转到主页面
     * @return
     */
    @GetMapping("/main")
    public String main(Model model){
        //获取用户信息
        Object userMsg = redisUtils.getByKey("loginUser");
        if(null != userMsg){
            model.addAttribute("loginUser",(KgcUser)userMsg);
        }else{
            return "error";
        }
        //查询菜单
        KgcUser user = (KgcUser)userMsg;
        List<KgcMenu> list = kgcMenuService.queryMenuList(user.getId());
        model.addAttribute("menuList", list);
        return "main";
    }
}
