package com.example.controller;

import com.example.service.SendMailService;
import com.example.service.SmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Auther: wangxiaodan
 * @Date:2021/3/26 9:07
 * @Description:
 **/
@Controller
public class TestController {
    @Autowired
    private SendMailService sendMailService;

    @Autowired
    private SmsService smsService;
    /**
     * 发邮件
     * @param msg
     * @param target
     * @return
     */
    @GetMapping("/sendMsg")
    @ResponseBody
    public String sendMsg(@RequestParam String msg, @RequestParam String target){
        System.out.println("=========发送邮件开始===========");
        sendMailService.sendMail(msg,target);
        System.out.println("=========发送邮件的方法已结束=========");
        return "ok";
    }

    /**
     * 邮件内容包含Html、附件
     * @param msg
     * @param target
     * @return
     */
    @GetMapping("/sendMsg1")
    @ResponseBody
    public String complexSendMail(@RequestParam String msg, @RequestParam String target){
        sendMailService.complexSendMail(msg,target);
        return "ok";
    }

    @GetMapping("/testSms")
    @ResponseBody
    public String testSms(@RequestParam String target){
       smsService.sendSms(target);
        return "ok";
    }

}
