package com.example.controller;

import com.example.dto.Page;
import com.example.pojo.KgcUser;
import com.example.service.KgcUserService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Auther: wangxiaodan
 * @Date:2021/4/1 10:09
 * @Description:
 **/
@RestController
public class KgcUserController {

    @Autowired
    private KgcUserService kgcUserService;

    /**
     * 自定义分页
     * @param page
     * @return
     */
    @GetMapping("/test")
    public List<KgcUser> queryUserByPage(Page page){
        return  kgcUserService.queryUserByPage(page);
    }
    /**
     * 使用分页插件
     * @param page
     * @return
     */
    @GetMapping("/testPageHelper")
    public PageInfo<KgcUser> queryUserByPageHelper(Page page){
        return  kgcUserService.queryUserByPageHelper(page);
    }

}
